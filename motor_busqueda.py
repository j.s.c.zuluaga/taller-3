import re
import os
import importlib.util
from fastapi import FastAPI

def normalizar_texto(texto):
    texto_normalizado = texto.lower()
    texto_normalizado = re.sub(r'[^\w\s]', '', texto_normalizado)
    return texto_normalizado

def normalizar_archivo_py(archivo_entrada, archivo_salida):
    with open(archivo_entrada, 'r') as f:
        contenido = f.read()

    contenido_normalizado = re.sub(r"'([^']*)'", lambda m: f"'{normalizar_texto(m.group(1))}'", contenido)

    with open(archivo_salida, 'w') as f:
        f.write(contenido_normalizado)

archivo_entrada = 'mook.py'
archivo_salida = 'datos_normalizado.py'
normalizar_archivo_py(archivo_entrada, archivo_salida)

def map_a_decreto(archivo_entrada, archivo_salida):
    decretos_tutelas = {}

    nombre_modulo = os.path.splitext(os.path.basename(archivo_entrada))[0]
    spec = importlib.util.spec_from_file_location(nombre_modulo, archivo_entrada)
    datos_normalizados = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(datos_normalizados)

    datos = datos_normalizados.tutela

    for item in datos:
        decreto = item.get('decreto')
        if decreto:
            if decreto not in decretos_tutelas:
                decretos_tutelas[decreto] = []
            decretos_tutelas[decreto].append(item)

    with open(archivo_salida, 'w') as f:
        f.write('decretos_tutelas = {\n')
        num_decretos = len(decretos_tutelas)
        for i, (decreto, tutelas) in enumerate(decretos_tutelas.items()):
            f.write(f" 'decreto: '{decreto}', 'tutelas': {tutelas}")
            if i < num_decretos - 1:
                f.write(',')
            f.write('\n')
        f.write('}')

archivo_entrada = 'datos_normalizado.py'
archivo_salida = 'decretos_tutelas.py'
map_a_decreto(archivo_entrada, archivo_salida)

